from typing import Optional

from asyncpg import UniqueViolationError

from todo_list.db import database
from todo_list.db.schema.user import user_table
from todo_list.models.auth import UserAuth


async def fetch_user(username: str) -> Optional[UserAuth]:
    query = user_table.select().where(user_table.c.username == username)
    user_dict = await database.fetch_one(query)
    if user_dict:
        return UserAuth(**user_dict)


async def insert_user(username: str, password_hash: str) -> Optional[int]:
    query = user_table.insert(
        returning=[user_table.c.id]
    ).values(username=username, password_hash=password_hash)

    try:
        user_id = await database.execute(query)
    except UniqueViolationError:
        user_id = None

    return user_id


async def delete_user(user_id: int) -> Optional[int]:
    query = user_table.delete(
        returning=[user_table.c.id]
    ).where(
        user_table.c.id == user_id
    )

    deleted_user_id = await database.execute(query)

    return deleted_user_id


async def delete_user_by_name(username: str) -> bool:
    query = user_table.delete(
        returning=[user_table.c.id]
    ).where(
        user_table.c.username == username
    )

    deleted_user_id = await database.execute(query)

    return deleted_user_id is not None
