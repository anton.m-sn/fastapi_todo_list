from typing import List, Optional

from todo_list.db import database
from todo_list.db.schema.todo_item import todo_item_table
from todo_list.models.todo_item import TodoItem, TodoItemCreate


async def fetch_todo_items(user_id: int) -> List[TodoItem]:
    query = todo_item_table.select().where(
        todo_item_table.c.user_id == user_id
    ).order_by(todo_item_table.c.id)

    rows = await database.fetch_all(query)

    return [TodoItem(**row) for row in rows]


async def fetch_todo_item(user_id: int, item_id: int) -> Optional[TodoItem]:
    query = todo_item_table.select().where(
        todo_item_table.c.user_id == user_id
    ).where(
        todo_item_table.c.id == item_id
    )

    item_dict = await database.fetch_one(query)

    if item_dict:
        return TodoItem(**item_dict)


async def insert_todo_item(user_id: int, todo_item: TodoItemCreate) -> int:
    query = todo_item_table.insert().values(
        user_id=user_id, **todo_item.dict()
    )
    item_id = await database.execute(query)

    return item_id


async def update_todo_item(user_id: int, item_id: int, item: TodoItemCreate) -> bool:
    query = todo_item_table.update(
        returning=[todo_item_table.c.id]
    ).values(
        **item.dict(),
    ).where(
        todo_item_table.c.id == item_id
    ).where(
        todo_item_table.c.user_id == user_id,
    )

    updated_item_id = await database.execute(query)
    success = updated_item_id is not None

    return success


async def delete_todo_item(user_id: int, item_id: int) -> bool:
    query = todo_item_table.delete(
        returning=[todo_item_table.c.id]
    ).where(
        todo_item_table.c.id == item_id
    ).where(
        todo_item_table.c.user_id == user_id,
    )

    deleted_item_id = await database.execute(query)
    success = deleted_item_id is not None

    return success
