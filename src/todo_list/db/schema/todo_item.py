import sqlalchemy

from todo_list.db import metadata


todo_item_table = sqlalchemy.Table(
    'todo_item',
    metadata,
    sqlalchemy.Column('id', sqlalchemy.BigInteger, primary_key=True),
    sqlalchemy.Column('user_id', sqlalchemy.BigInteger,
                      sqlalchemy.ForeignKey('app_user.id', ondelete='CASCADE'), nullable=False),
    sqlalchemy.Column('title', sqlalchemy.Text, nullable=False),
    sqlalchemy.Column('description', sqlalchemy.Text, nullable=False),
)
