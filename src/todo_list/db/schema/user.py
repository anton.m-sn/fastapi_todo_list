import sqlalchemy

from todo_list.db import metadata


user_table = sqlalchemy.Table(
    'app_user',
    metadata,
    sqlalchemy.Column('id', sqlalchemy.BigInteger, primary_key=True),
    sqlalchemy.Column('username', sqlalchemy.Text, nullable=False, unique=True),
    sqlalchemy.Column('password_hash', sqlalchemy.Text),
)
