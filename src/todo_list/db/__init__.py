import databases
import sqlalchemy

from todo_list.config import DATABASE_URL


metadata = sqlalchemy.MetaData()
database = databases.Database(DATABASE_URL)


def register_db(app):

    @app.on_event('startup')
    async def startup():
        await database.connect()

    @app.on_event('shutdown')
    async def shutdown():
        await database.disconnect()
