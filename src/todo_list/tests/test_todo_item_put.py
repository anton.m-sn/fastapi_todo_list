import pytest

from todo_list.models.todo_item import TodoItemCreate
from todo_list.db.operations.todo_item import fetch_todo_item
from todo_list.tests.const import NOT_FOUND_RESPONSE_JSON, RANDOM_LARGE_ID
from todo_list.tests.fixtures.todo_item import first_item, another_user_item


@pytest.mark.asyncio
async def test_update_my_todo_item(test_app, created_user_auth_headers, created_user_id, first_item):
    first_item.title = 'new title'
    first_item.description = 'new description'
    update_item = TodoItemCreate(**first_item.dict())

    response = await test_app.put(
        f"/api/todo_items/{first_item.id}",
        headers=created_user_auth_headers,
        json=update_item.dict(),
    )

    assert response.status_code == 200

    assert response.json() == first_item.dict()

    item_from_db = await fetch_todo_item(user_id=created_user_id, item_id=first_item.id)
    assert item_from_db == first_item


@pytest.mark.asyncio
async def test_update_another_user_todo_item(test_app, created_user_auth_headers, another_user_item, another_user_id):
    item = another_user_item.copy()
    item.title = 'new title'
    item.description = 'new description'
    update_item = TodoItemCreate(**item.dict())

    response = await test_app.put(
        f"/api/todo_items/{item.id}",
        headers=created_user_auth_headers,
        json=update_item.dict(),
    )

    assert response.status_code == 404

    assert response.json() == NOT_FOUND_RESPONSE_JSON

    item_from_db = await fetch_todo_item(user_id=another_user_id, item_id=another_user_item.id)
    assert item_from_db == another_user_item


@pytest.mark.asyncio
async def test_update_nonexistent_todo_item(test_app, created_user_auth_headers):
    update_item = TodoItemCreate(title='title', description='desc')

    response = await test_app.put(
        f"/api/todo_items/{RANDOM_LARGE_ID}",
        headers=created_user_auth_headers,
        json=update_item.dict(),
    )

    assert response.status_code == 404

    assert response.json() == NOT_FOUND_RESPONSE_JSON
