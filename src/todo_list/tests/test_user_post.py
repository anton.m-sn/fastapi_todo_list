import pytest

from todo_list.tests.conftest import USERS
from todo_list.db.operations.user import delete_user_by_name
from todo_list.models.user import UserCreate


@pytest.mark.asyncio
async def test_create_new_user(test_app):
    user_to_create = UserCreate(username='Eminem', password='yoyoyo')

    response = await test_app.post(
        "/api/users/",
        json=user_to_create.dict(),
    )

    assert response.status_code == 200

    assert response.json() == {'username': user_to_create.username}

    await delete_user_by_name(user_to_create.username)


@pytest.mark.asyncio
async def test_create_existing_user(test_app, created_user_id):
    user_to_create = UserCreate(username=USERS[0]['username'], password='another_password')

    response = await test_app.post(
        "/api/users/",
        json=user_to_create.dict(),
    )

    assert response.status_code == 409

    assert response.json() == {'detail': 'This username is already taken'}
