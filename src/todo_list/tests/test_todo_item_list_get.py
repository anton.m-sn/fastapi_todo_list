import pytest

from todo_list.tests.fixtures.todo_item import first_item, another_user_item


@pytest.mark.asyncio
async def test_get_empty_todo_list(test_app, created_user_auth_headers, another_user_item):
    response = await test_app.get("/api/todo_items/", headers=created_user_auth_headers)

    assert response.status_code == 200

    assert response.json() == []


@pytest.mark.asyncio
async def test_get_todo_list(test_app, created_user_auth_headers, first_item, another_user_item):
    response = await test_app.get("/api/todo_items/", headers=created_user_auth_headers)

    assert response.status_code == 200

    assert response.json() == [first_item.dict()]
