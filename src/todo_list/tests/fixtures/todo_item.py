import pytest

from todo_list.db.operations.todo_item import insert_todo_item, delete_todo_item
from todo_list.models.todo_item import TodoItemCreate, TodoItem


FIRST_ITEM_CREATE = TodoItemCreate(
    title='shopping',
    description='Buy jeans and a t-shirt.',
)
ANOTHER_USER_ITEM_CREATE = TodoItemCreate(
    title='groceries',
    description='milk, bread, butter',
)


@pytest.fixture()
async def first_item(test_app, created_user_id):
    item_id = await insert_todo_item(
        created_user_id,
        FIRST_ITEM_CREATE,
    )

    yield TodoItem(
        id=item_id,
        **FIRST_ITEM_CREATE.dict(),
    )

    await delete_todo_item(created_user_id, item_id)


@pytest.fixture()
async def another_user_item(test_app, another_user_id):
    item_id = await insert_todo_item(
        another_user_id,
        ANOTHER_USER_ITEM_CREATE,
    )

    yield TodoItem(
        id=item_id,
        **ANOTHER_USER_ITEM_CREATE.dict(),
    )

    await delete_todo_item(another_user_id, item_id)
