import pytest

from todo_list.tests.const import RANDOM_LARGE_ID, NOT_FOUND_RESPONSE_JSON
from todo_list.tests.fixtures.todo_item import first_item, another_user_item


@pytest.mark.asyncio
async def test_get_my_todo_item(test_app, created_user_auth_headers, first_item):
    response = await test_app.get(f"/api/todo_items/{first_item.id}", headers=created_user_auth_headers)

    assert response.status_code == 200

    assert response.json() == first_item.dict()


@pytest.mark.asyncio
async def test_get_another_user_todo_item(test_app, created_user_auth_headers, another_user_item):
    response = await test_app.get(f"/api/todo_items/{another_user_item.id}", headers=created_user_auth_headers)

    assert response.status_code == 404

    assert response.json() == NOT_FOUND_RESPONSE_JSON


@pytest.mark.asyncio
async def test_get_nonexistent_todo_item(test_app, created_user_auth_headers):
    response = await test_app.get(f"/api/todo_items/{RANDOM_LARGE_ID}", headers=created_user_auth_headers)

    assert response.status_code == 404

    assert response.json() == NOT_FOUND_RESPONSE_JSON
