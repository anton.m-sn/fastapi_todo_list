import asyncio
import os

import pytest
from asgi_lifespan import LifespanManager
from httpx import AsyncClient
from oauthlib.oauth2 import LegacyApplicationClient
from requests_oauthlib import OAuth2Session

from todo_list.auth import create_user
from todo_list.db.operations.user import delete_user
from todo_list.main import app

USERS = [
    {'username': 'Ivan', 'password': 'qwerty'},
    {'username': 'Lola', 'password': '12345'},
]

BASE_URL = 'http://localhost/'


@pytest.fixture(scope="session")
def event_loop():
    loop = asyncio.get_event_loop()
    yield loop
    loop.close()


@pytest.fixture(scope="session")
async def test_app():
    async with LifespanManager(app):
        async with AsyncClient(app=app, base_url=BASE_URL) as client:
            yield client


@pytest.fixture(scope="session")
async def created_user_id(test_app):
    user_id = await create_user(**USERS[0])
    assert user_id is not None
    yield user_id
    await delete_user(user_id)


@pytest.fixture(scope="session")
async def another_user_id(test_app):
    user_id = await create_user(**USERS[1])
    assert user_id is not None
    yield user_id
    await delete_user(user_id)


@pytest.fixture(scope="session")
def created_user_auth_headers(created_user_id):
    yield get_auth_headers(**USERS[0])


@pytest.fixture(scope="session")
def another_user_auth_headers(created_user_id):
    yield get_auth_headers(**USERS[1])


def get_auth_headers(username: str, password: str):
    oauth = OAuth2Session(client=LegacyApplicationClient(client_id=''))
    os.environ['OAUTHLIB_INSECURE_TRANSPORT'] = '1'
    token = oauth.fetch_token(
        token_url=f'{BASE_URL}token/',
        username=username,
        password=password,
    )
    token_type = token['token_type']
    access_token = token['access_token']
    headers = {'Authorization': f'{token_type} {access_token}'}

    return headers
