import pytest

from todo_list.models.todo_item import TodoItemCreate, TodoItem
from todo_list.db.operations.todo_item import fetch_todo_item, delete_todo_item


@pytest.mark.asyncio
async def test_create_todo_item(test_app, created_user_auth_headers, created_user_id):
    item_to_create = TodoItemCreate(title='The title', description='Description goes here.')

    response = await test_app.post(
        f"/api/todo_items",
        headers=created_user_auth_headers,
        json=item_to_create.dict(),
    )

    assert response.status_code == 200

    created_item = TodoItem(**response.json())
    assert TodoItemCreate(**created_item.dict()) == item_to_create

    item_from_db = await fetch_todo_item(user_id=created_user_id, item_id=created_item.id)
    assert item_from_db == created_item

    await delete_todo_item(user_id=created_user_id, item_id=created_item.id)
