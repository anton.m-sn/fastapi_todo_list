import pytest

from todo_list.db.operations.todo_item import fetch_todo_item
from todo_list.tests.const import OK_RESPONSE_JSON, NOT_FOUND_RESPONSE_JSON, RANDOM_LARGE_ID
from todo_list.tests.fixtures.todo_item import first_item, another_user_item


@pytest.mark.asyncio
async def test_delete_my_todo_item(test_app, created_user_auth_headers, created_user_id, first_item):
    response = await test_app.delete(f"/api/todo_items/{first_item.id}", headers=created_user_auth_headers)

    assert response.status_code == 200

    assert response.json() == OK_RESPONSE_JSON

    item_from_db = await fetch_todo_item(user_id=created_user_id, item_id=first_item.id)
    assert item_from_db is None


@pytest.mark.asyncio
async def test_delete_another_user_todo_item(test_app, created_user_auth_headers, another_user_item):
    response = await test_app.delete(f"/api/todo_items/{another_user_item.id}", headers=created_user_auth_headers)

    assert response.status_code == 404

    assert response.json() == NOT_FOUND_RESPONSE_JSON


@pytest.mark.asyncio
async def test_delete_nonexistent_todo_item(test_app, created_user_auth_headers):
    response = await test_app.delete(f"/api/todo_items/{RANDOM_LARGE_ID}", headers=created_user_auth_headers)

    assert response.status_code == 404

    assert response.json() == NOT_FOUND_RESPONSE_JSON
