from fastapi import Depends, APIRouter
from fastapi.security import OAuth2PasswordRequestForm

from todo_list.auth import get_unauthorized_exception, authenticate_user, create_access_token
from todo_list.models.auth import Token, UserAuth


router = APIRouter()


@router.post('/token/', response_model=Token)
async def login_for_access_token(form_data: OAuth2PasswordRequestForm = Depends()):
    user_auth: UserAuth = await authenticate_user(form_data.username, form_data.password)
    if not user_auth:
        raise get_unauthorized_exception('Incorrect username or password')
    access_token = create_access_token(user_auth.username)

    return {'access_token': access_token, 'token_type': 'bearer'}
