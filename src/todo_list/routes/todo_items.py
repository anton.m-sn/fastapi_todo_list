from typing import List

from fastapi import Depends, APIRouter, HTTPException, status

from todo_list.auth import get_current_user
from todo_list.db.operations.todo_item import (
    delete_todo_item,
    fetch_todo_item,
    fetch_todo_items,
    insert_todo_item,
    update_todo_item,
)
from todo_list.models.todo_item import TodoItem, TodoItemCreate
from todo_list.models.user import User


router = APIRouter()


@router.get("/todo_items", response_model=List[TodoItem])
async def get_todo_list(current_user: User = Depends(get_current_user)):
    return await fetch_todo_items(current_user.id)


@router.get("/todo_items/{item_id}", response_model=TodoItem)
async def get_todo(item_id: int, current_user: User = Depends(get_current_user)):
    todo_item = await fetch_todo_item(user_id=current_user.id, item_id=item_id)
    if not todo_item:
        raise HTTPException(status_code=status.HTTP_404_NOT_FOUND)

    return todo_item


@router.post("/todo_items", response_model=TodoItem)
async def create_todo(todo_item: TodoItemCreate, current_user: User = Depends(get_current_user)):
    todo_id = await insert_todo_item(current_user.id, todo_item)
    created_todo_item = TodoItem(id=todo_id, **todo_item.dict())

    return created_todo_item


@router.put("/todo_items/{item_id}", response_model=TodoItem)
async def update_todo(item_id: int, todo_item: TodoItemCreate, current_user: User = Depends(get_current_user)):
    success = await update_todo_item(
        user_id=current_user.id,
        item_id=item_id,
        item=todo_item,
    )

    if not success:
        raise HTTPException(status_code=status.HTTP_404_NOT_FOUND)

    updated_todo_item = TodoItem(id=item_id, **todo_item.dict())

    return updated_todo_item


@router.delete("/todo_items/{item_id}")
async def delete_todo(item_id: int, current_user: User = Depends(get_current_user)):
    success = await delete_todo_item(user_id=current_user.id, item_id=item_id)

    if not success:
        raise HTTPException(status_code=status.HTTP_404_NOT_FOUND)

    return 'OK'
