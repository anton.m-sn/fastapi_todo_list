from fastapi import APIRouter, HTTPException, status

from todo_list.auth import create_user
from todo_list.models.user import UserCreate


router = APIRouter()


@router.post('/users/')
async def signup(user: UserCreate):
    user_id = await create_user(user.username, user.password)
    if user_id is None:
        raise HTTPException(status_code=status.HTTP_409_CONFLICT,
                            detail='This username is already taken')
    return {'username': user.username}
