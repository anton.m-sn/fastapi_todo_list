from datetime import datetime, timedelta
from typing import Optional

from fastapi import HTTPException, status, Depends
from fastapi.security import OAuth2PasswordBearer
from jose import jwt, ExpiredSignatureError, JWTError
from passlib.context import CryptContext

from todo_list.config import JWT_EXPIRE_MINUTES, JWT_SECRET_KEY, JWT_ALGORITHM
from todo_list.db.operations.user import fetch_user, insert_user
from todo_list.models.auth import UserAuth


pwd_context = CryptContext(schemes=['bcrypt'], deprecated='auto')
oauth2_scheme = OAuth2PasswordBearer(tokenUrl='token')


def get_unauthorized_exception(detail: str):
    return HTTPException(
        status_code=status.HTTP_401_UNAUTHORIZED,
        detail=detail,
        headers={'WWW-Authenticate': 'Bearer'},
    )


def verify_password(password, password_hash):
    return pwd_context.verify(password, password_hash)


def get_password_hash(password):
    return pwd_context.hash(password)


async def create_user(username: str, password: str) -> Optional[int]:
    password_hash = get_password_hash(password)
    return await insert_user(username, password_hash)


async def authenticate_user(username: str, password: str) -> Optional[UserAuth]:
    user_auth: UserAuth = await fetch_user(username)
    if not user_auth:
        return None
    if not verify_password(password, user_auth.password_hash):
        return None
    return user_auth


def create_access_token(username: str):
    expiry = datetime.utcnow() + timedelta(minutes=JWT_EXPIRE_MINUTES)
    data = {'sub': username, 'exp': expiry}
    encoded_jwt = jwt.encode(data, JWT_SECRET_KEY, algorithm=JWT_ALGORITHM)
    return encoded_jwt


async def get_current_user(token: str = Depends(oauth2_scheme)) -> UserAuth:
    try:
        payload = jwt.decode(token, JWT_SECRET_KEY, algorithms=[JWT_ALGORITHM])
    except ExpiredSignatureError:
        raise get_unauthorized_exception('Token expired')
    except JWTError:
        raise get_unauthorized_exception('Could not validate credentials')

    username: str = payload.get('sub')
    if username is None:
        raise get_unauthorized_exception('Could not validate credentials')

    user_auth: UserAuth = await fetch_user(username)
    if user_auth is None:
        raise get_unauthorized_exception('Could not validate credentials')

    return user_auth
