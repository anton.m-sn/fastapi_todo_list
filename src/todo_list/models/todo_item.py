from pydantic import BaseModel


class TodoItem(BaseModel):
    id: int
    title: str
    description: str


class TodoItemCreate(BaseModel):
    title: str
    description: str
