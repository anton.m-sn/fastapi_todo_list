from pydantic import BaseModel

from todo_list.models.user import User


class Token(BaseModel):
    access_token: str
    token_type: str


class UserAuth(User):
    password_hash: str
