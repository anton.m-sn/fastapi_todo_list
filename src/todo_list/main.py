from fastapi import FastAPI

from todo_list.db import register_db
from todo_list.routes import auth, todo_items, user


app = FastAPI()
app.include_router(auth.router)
app.include_router(todo_items.router, prefix='/api')
app.include_router(user.router, prefix='/api')
register_db(app)


if __name__ == '__main__':
    import uvicorn
    uvicorn.run(app, host='0.0.0.0', port=8000)
