## Simple todo-list backend application

Implemented with Python 3.8, FastAPI, Postgres, SQLAlchemy Core and Alembic.

Functionality:
* User signup and login (each user has access only to his own to-do items)
* Create, Update, Delete, Get a to-do item
* Get all to-do items

How to use:
1) Run `docker-compose up -d --build` from the project directory
2) Run tests: `docker-compose exec api pytest .`
3) Go to http://127.0.0.1/docs# to explore the API